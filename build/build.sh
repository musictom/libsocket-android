#!/bin/bash
U32=0 #编译64位arm时 U32=0   编译32位arm时 U32=1 其他参数不需要变动
TARGET=android-24
HOST=darwin-x86_64
TOOL=aarch64-linux-android #arm-linux-androideabi  aarch64-linux-android

ARCH=arch-arm64 #arch-arm arch-arm64
if [ $U32 -ne 0 ]; then
	echo "32位arm"
    TOOL=arm-linux-androideabi
    ARCH=arch-arm
fi
TOOLCHAIN=$TOOL-4.9

export NDK_ROOT=/Users/musictom/Library/Android/sdk/ndk-bundle/
export SYSROOT="$NDK_ROOT/platforms/$TARGET/$ARCH"
export CPPFLAGS="-I$NDK_ROOT/platforms/$TARGET/$ARCH/usr/include -I$NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/user/include"
export LDFLAGS="-L$NDK_ROOT/platforms/$TARGET/$ARCH/usr/lib --sysroot=$SYSROOT"

rm -f *.so *.o
$NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/bin/$TOOL-gcc -Wall -fPIC -O2 -c ../c/inet/libinetsocket.c $CPPFLAGS
$NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/bin/$TOOL-gcc -Wall -fPIC -O2 -c ../c/unix/libunixsocket.c $CPPFLAGS
$NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/bin/$TOOL-gcc -shared -o libsocket.so libinetsocket.o libunixsocket.o $LDFLAGS
rm -f *.o

cp *.so $NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/user/lib/
mkdir $NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/user/include/socket
cp ../headers/*.hpp $NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/user/include/socket/
cp ../headers/*.h $NDK_ROOT/toolchains/$TOOLCHAIN/prebuilt/$HOST/user/include/socket/