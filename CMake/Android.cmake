#cd POCO-1.7.8-ALL
#mkdir BUILD && cd BUILD
#cmake -DCMAKE_CXX_FLAGS='-std=c++11 -frtti -fexceptions -fpermissive'  ..
#make -j4
set(CMAKE_BUILD_TYPE Release)
set(BUILD_SHARED_LIBS true)
set(ARCH arch-arm) #arch-arm  arch-arm64  arch-mips  arch-mips64  arch-x86  arch-x86_64
set(ANDROID 1)
set(HOST darwin-x86_64)
set(HOME /Users)
if (${HOST} STREQUAL "linux-x86_64")
	set(HOME /home)
else()
endif()

set(CMAKE_CXX_FLAGS "-frtti -fexceptions -fpic -fpermissive -I${CMAKE_CURRENT_SOURCE_DIR}")
set(CMAKE_CXX_FLAGS_DEBUG -fexceptions)

#ANDROID_ABI : 
#"armeabi-v7a" (default), "armeabi", "armeabi-v7a with NEON", 
#"armeabi-v7a with VFPV3", "armeabi-v6 with VFP", "x86", "mips", "arm64-v8a", "x86_64", "mips64"

#ANDROID_TOOLCHAIN_NAME
#aarch64-linux-android-4.9       llvm                            mipsel-linux-android-4.9        x86_64-4.9
#arm-linux-androideabi-4.9       mips64el-linux-android-4.9      x86-4.9

if (${ARCH} STREQUAL "arch-arm64")
    set(TOOL aarch64-linux-android)
    set(ANDROID_ABI "arm64-v8a")
    set(ANDROID_TOOLCHAIN_NAME ${TOOL}-4.9)
elseif (${ARCH} STREQUAL "arch-arm")
    set(TOOL arm-linux-androideabi)
    set(ANDROID_ABI "armeabi-v7a")  #"armeabi-v7a" (default), "armeabi", "armeabi-v7a with NEON"
    set(ANDROID_TOOLCHAIN_NAME ${TOOL}-4.9)
elseif (${ARCH} STREQUAL "arch-mips64")
    set(TOOL mips64el-linux-android)
    set(ANDROID_ABI "mips64")
    set(ANDROID_TOOLCHAIN_NAME ${TOOL}-4.9)
elseif (${ARCH} STREQUAL "arch-mips")
    set(TOOL mipsel-linux-android)
    set(ANDROID_ABI "mips")
    set(ANDROID_TOOLCHAIN_NAME ${TOOL}-4.9)
elseif (${ARCH} STREQUAL "arch-x86")
    set(TOOL i686-linux-android)
    set(ANDROID_ABI "x86")
    set(ANDROID_TOOLCHAIN_NAME x86-4.9)
elseif (${ARCH} STREQUAL "arch-x86_64")
    set(TOOL x86_64-linux-android)
    set(ANDROID_ABI "x86_64")
    set(ANDROID_TOOLCHAIN_NAME x86_64-4.9)
endif()
message(state 'TOOL:'${TOOL})
message(state 'ANDROID_ABI:'${ANDROID_ABI})



if (${HOST} STREQUAL "linux-x86_64")
	set(ANDROID_NDK ${HOME}/musictom/Android/Sdk/ndk-bundle)
else()
	set(ANDROID_NDK ${HOME}/musictom/Library/Android/Sdk/ndk-bundle)
endif()

set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/CMake/android.toolchain.cmake)
#set(ANDROID_STL stlport_static)


set(ANDROID_NATIVE_API_LEVEL android-24)
set(CMAKE_C_COMPILER ${ANDROID_NDK}/toolchains/${ANDROID_TOOLCHAIN_NAME}/prebuilt/${HOST}/bin/${TOOL}-gcc)
set(CMAKE_CXX_COMPILER ${ANDROID_NDK}/toolchains/${ANDROID_TOOLCHAIN_NAME}/prebuilt/${HOST}/bin/${TOOL}-g++)
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${HOME}/musictom/libs/android/${ARCH}/usr/lib/${ANDROID_ABI})
set(HEADER_DIRECTORY ${HOME}/musictom/libs/android/${ARCH}/usr/include/my)

MACRO(INSTALL_HEADERS_WITH_DIRECTORY HEADER_LIST)
    FOREACH(HEADER ${${HEADER_LIST}})
        STRING(REGEX MATCH "(.\\\*)\\\[/\\\]" DIR ${HEADER})
        INSTALL(FILES ${HEADER} DESTINATION ${HEADER_DIRECTORY}/${DIR})
    ENDFOREACH(HEADER)
ENDMACRO(INSTALL_HEADERS_WITH_DIRECTORY)

#add_compile_options(-std=c++11)
#message(STATUS "optional:-std=c++11") 
